#!/usr/local/bin/python3

import sys, argparse, os
from pathlib import Path
from obspy.core.utcdatetime import UTCDateTime
from obspy import read
from shutil import copy

def main():
  parser = argparse.ArgumentParser(description='Archive miniseed files')
  parser.add_argument('-i', '--input', dest='indir', required=True)
  parser.add_argument('-o', '--outdir', dest='outdir', required=True)
  parser.add_argument('-b', '--bakdir', dest='bakdir', required=False)
  args = parser.parse_args()

  # Slice traces from source directory by HOUR
  now = UTCDateTime.now()
  t = UTCDateTime(now.year, now.month, now.day, now.hour, 0, 0, 0)
  t1 = t - 60*60
  t2 = t - 0.000001 # one microsecond before end hour

  indirlen = len(args.indir.split(os.sep))
  outpath = args.outdir.split(os.sep)
  for root, dirs, files in os.walk(args.indir):
    path = root.split(os.sep)
    cpath = path[indirlen:]
    if len(cpath) == 3:
      Path(('/' if args.outdir.startswith('/') else '') + os.path.join(*(outpath + cpath))).mkdir(parents=True, exist_ok=True)
      try:
        stream = read(root + '/*')
        result = stream.slice(t1, t2)
        if result.count() == 0:
          print(root, 'nothing matches past hour')
        else:
          target = ('/' if args.outdir.startswith('/') else '') + os.path.join(*(outpath + cpath + [t1.__str__() + '.mseed']))
          print(root, ' -> ', target)
          result.write(target)

        # Backup or remove obsolete source files
        for file in files:
          fpath = os.path.join(root,file)
          if os.stat(fpath).st_mtime < now - 7200:
            if args.bakdir != None:
              bakpath = args.bakdir.split(os.sep)
              Path(('/' if args.bakdir.startswith('/') else '') + os.path.join(*(bakpath + cpath))).mkdir(parents=True, exist_ok=True)
              # os.rename(fpath, ('/' if args.bakdir.startswith('/') else '') + os.path.join(*(bakpath + cpath + [file])))
              copy(fpath, ('/' if args.bakdir.startswith('/') else '') + os.path.join(*(bakpath + cpath + [file])))
            os.remove(fpath)

      except Exception as ex:
        print(root, 'is empty', type(ex))
        print(ex)


if __name__ == "__main__":
   main()
