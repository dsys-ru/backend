from obspy.clients.seedlink.easyseedlink import EasySeedLinkClient
from threading import Thread
from pathlib import Path
from globals import clients, traces
import time
import jsonpickle
from os import rename

class Poller(EasySeedLinkClient):
    def on_data(self, trace):
        # print(trace)
        # https://github.com/obspy/obspy/issues/2683
        try:
            millis = str(round(time.time() * 1000))

            # trace.write(self.dir + '/' + millis + '.mseed', format='MSEED')
            trace.write(self.temp + '.mseed', format='MSEED')
            rename(self.temp + '.mseed', self.dir + '/' + millis + '.mseed')

            key = '/ws/stream/' + trace.get_id()
            if clients.get(key) == None:
                return
            encoded = jsonpickle.encode({ "trace": str(trace), "data": trace.data.tolist() })
            for client in clients[key]:
                client.sendMessage(encoded)

        except Exception as ex:
            print('Exception in trace', trace)
            print('Poller ' + self.dir, ex)

def start_poller(url, network, station, seedname):
    dir = 'data/' + network + '/' + station + '/' + seedname
    Path(dir).mkdir(parents=True, exist_ok=True)
    client = Poller(url)
    client.dir = dir
    client.temp = 'data/' + network + '-' + station + '-' + seedname
    client.select_stream(network, station, seedname)
    thread = Thread(target=lambda: client.run())
    thread.daemon = True
    thread.start()
    return client
