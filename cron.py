import os
from datetime import datetime, timedelta
from subprocess import Popen, DEVNULL

def getBackendHome():
    try:
        return os.environ['BACKEND_HOME']
    except Exception:
        return '/mnt/apt/backend'

def threshold():
    now = datetime.now()
    if now.minute < 10:
        return datetime(now.year, now.month, now.day, now.hour, 10, 0, tzinfo=now.tzinfo)
    else:
        return datetime(now.year, now.month, now.day, now.hour, 10, 0, tzinfo=now.tzinfo) + timedelta(hours = 1)

# Crontab to spawn processes
def cron(e):
    th = threshold()

    while not e.is_set():
        if datetime.now() >= th:
            print('Spawn archiver process')
            th = threshold()
            backendHome = getBackendHome()
            Popen([
              '/usr/bin/nohup /usr/bin/python3 %(bh)s/archiver.py -i %(bh)s/data -o %(bh)s/arch -b %(bh)s/bak > %(bh)s/log/archiver.log' % { "bh": backendHome }
              ], stdout=DEVNULL, stderr=DEVNULL, shell=True)
        e.wait(60) # One minute
