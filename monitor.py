import xmltodict, json
import jsonpickle
from time import sleep
from obspy.clients.seedlink.easyseedlink import create_client
from globals import devices, streams
from poller import start_poller

# Keep checking for availabile devices
def monitor(e):
    while not e.is_set():
        with open('data/devices.json') as json_file:
            _devices = jsonpickle.decode(json_file.read())
            for device in _devices:
                try:
                    seedClient = create_client(device['host'])
                    xml = xmltodict.parse(seedClient.get_info('STREAMS'))
                    seedlink = xml['seedlink']
                    device['organization'] = seedlink['@organization']
                    device['software'] = seedlink['@software']
                    device['started'] = seedlink['@started']
                    device['active'] = True

                    station = seedlink['station']
                    name = station['@name']
                    network = station['@network']

                    for stream in station['stream']:
                        seedname = stream['@seedname']
                        key = network + '|' + name + '|' + seedname
                        if streams.get(key) == None:
                            streams[key] = start_poller(device['host'], network, name, seedname)
                            print('Created poller for stream ' + key)

                except Exception as ex:
                    print('Error in device monitor', str(ex))
                    device['active'] = False

            devices.clear()
            devices.extend(_devices)

        with open('data/devices.json', 'w') as outfile:
            outfile.write(jsonpickle.encode(_devices))

        e.wait(60)
