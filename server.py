import jsonpickle, signal
from os import environ
from SimpleWebSocketServer import SimpleWebSocketServer, WebSocket
from monitor import monitor
from cron import cron
from threading import Event, Thread
from globals import streams
from publisher import Publisher

jsonpickle.set_encoder_options('json', sort_keys=True, indent=2)

def websokets(e):
    server = SimpleWebSocketServer('', 9090, Publisher)
    while not e.is_set():
        server.serveonce()


def main():
    exit = Event()
    for sig in ('TERM', 'HUP', 'INT'):
        signal.signal(getattr(signal, 'SIG'+sig), lambda signum, frame: exit.set())

    server = Thread(target=websokets, args=[exit])
    server.daemon = True
    server.start()

    t1 = Thread(target=monitor, args=[exit])
    t1.daemon = True
    t1.start()

    t2 = None # Do not run cron thread if crontab is present
              # (see https://bitbucket.org/dsys-ru/docker)
    if ('CRONTAB' not in environ):
        t2 = Thread(target=cron, args=[exit])
        t2.daemon = True
        t2.start()

    t1.join()
    if (t2 != None):
        t2.join()

    for stream in streams.values():
        stream.close()

    print('Server terminated')


if __name__ == "__main__":
    main()
