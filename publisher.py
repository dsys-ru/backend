from obspy.clients.seedlink.easyseedlink import create_client
from SimpleWebSocketServer import WebSocket
from globals import devices, clients
import jsonpickle, xmltodict, json, re
from obspy.core.utcdatetime import UTCDateTime
from obspy import read
from os import path

class DeviceAlreadyRegistered(Exception):
    def __init__(self):
        self.message = 'The host is already registered'

def addDevice(host, port):
    for device in devices:
        if device['host'] == host:
            raise DeviceAlreadyRegistered
    device = { 'host': host, 'port': port };
    seedClient = create_client(host)
    xml = xmltodict.parse(seedClient.get_info('ID'))
    seedlink = xml['seedlink']
    device['organization'] = seedlink['@organization']
    device['software'] = seedlink['@software']
    device['started'] = seedlink['@started']
    device['active'] = True
    devices.append(device)
    with open('data/devices.json', 'w') as outfile:
        outfile.write(jsonpickle.encode(devices))
    print('Device added', device)

def removeDevice(host, port):
    device = next(filter(lambda x: x['host'] == host and x['port'] == port, devices))
    devices.remove(device)
    with open('data/devices.json', 'w') as outfile:
        outfile.write(jsonpickle.encode(devices))
    print('Device removed', device)

def getStreams(host, port):
    device = { 'host': host, 'port': port }
    seedClient = create_client(host)
    xml = xmltodict.parse(seedClient.get_info('STREAMS'))
    seedlink = xml['seedlink']
    device['organization'] = seedlink['@organization']
    device['software'] = seedlink['@software']
    device['started'] = seedlink['@started']
    device['active'] = True
    station = seedlink['station']
    device['station'] = station['@name']
    device['network'] = station['@network']
    device['streams'] = []
    for stream in station['stream']:
        seedname = stream['@seedname']
        device['streams'].append({ 'seedname': seedname })
    return device


class Publisher(WebSocket):
    def handleMessage(self):
        message = jsonpickle.decode(self.data)
        print('Publisher::handleMessage', message)
        if message['message'] == 'get-chart':
            seconds = message['chart']['seconds']
            try:
                t = UTCDateTime.now()
                t1 = t - 60*seconds
                t2 = t - 0.000001 # one microsecond before end
                key = re.search(r'^/ws/stream/(.+)$', self.request.path).group(1)
                [network, station, channel, seedname] = key.split('.')
                stream = read(path.join('data', network, station, seedname, '*'), format='MSEED', starttime=t1, endtime=t2)
                traces = list(map(lambda trace: { "trace": str(trace), "data": trace.data.tolist() }, stream.traces))
                encoded = jsonpickle.encode({ "traces": traces })
                self.sendMessage(encoded)
            except Exception as ex:
                print(ex)

        elif self.request.path == '/ws/devices':
            if message['message'] == 'add-device':
                # Register a new device ( 192.168.1.111 )
                device = message['device']
                host = device['host']
                port = device['port']
                try:
                    addDevice(host, port)
                    encoded = jsonpickle.encode(devices)
                    for client in clients[self.request.path]:
                        client.sendMessage(encoded)
                except Exception as ex:
                    encoded = jsonpickle.encode({ 'error': ex.message, 'title': 'Failed to add device' })
                    self.sendMessage(encoded)

            elif message['message'] == 'remove-device':
                # Register a device
                device = message['device']
                host = device['host']
                port = device['port']
                try:
                    removeDevice(host, port)
                    encoded = jsonpickle.encode(devices)
                    for client in clients[self.request.path]:
                        client.sendMessage(encoded)
                except Exception as ex:
                    encoded = jsonpickle.encode({ 'error': ex.message, 'title': 'Failed to remove device' })
                    self.sendMessage(encoded)

        elif re.match(r'^/ws/device/(.+)$', self.request.path):
            if message['message'] == 'get-streams':
                device = message['device']
                host = device['host']
                port = device['port']
                try:
                    streams = getStreams(host, port)
                    encoded = jsonpickle.encode(streams)
                    for client in clients[self.request.path]:
                        client.sendMessage(encoded)
                except Exception as ex:
                    encoded = jsonpickle.encode({ 'error': ex.message, 'title': 'Failed to add device' })
                    self.sendMessage(encoded)

    def handleConnected(self):
        print('Publisher::handleConnected', self)

        if clients.get(self.request.path) == None:
            clients[self.request.path] = []
        clients[self.request.path].append(self)
        if self.request.path == '/ws/devices':
            self.sendMessage(jsonpickle.encode(devices))

        elif re.match(r'^/ws/stream/(.+)$', self.request.path):
            key = re.search(r'^/ws/stream/(.+)$', self.request.path).group(1)
            # Get the traces from data/network/station/seedname folder
            [network, station, channel, seedname] = key.split('.')
            # print('Connected for data', network, station, channel, seedname)
            try:
                t = UTCDateTime.now()
                t1 = t - 60*1 # one minute by default
                t2 = t - 0.000001 # one microsecond before end
                stream = read(path.join('data', network, station, seedname, '*'), format='MSEED', starttime=t1, endtime=t2)
                traces = list(map(lambda trace: { "trace": str(trace), "data": trace.data.tolist() }, stream.traces))
                encoded = jsonpickle.encode({ "traces": traces })
                self.sendMessage(encoded)
            except Exception as ex:
                print(ex)

    def handleClose(self):
        print('Publisher::handleClose', self)
        clients[self.request.path].remove(self)
        print(self.request.path, 'closed')
