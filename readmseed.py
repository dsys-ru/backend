from sys import argv
from obspy import read

def main(path):
    mseed = read(path)
    print(mseed)

if __name__ == "__main__":
    main(argv[1])
